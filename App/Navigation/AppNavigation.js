import { StackNavigator } from 'react-navigation'
import ConverterScreen from '../Containers/ConverterScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  ConverterScreen: { screen: ConverterScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'ConverterScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
