import { put } from 'redux-saga/effects'
import RatesActions from '../Redux/RatesRedux'

export function * startup (action) {
  yield put(RatesActions.ratesRequest())
}
