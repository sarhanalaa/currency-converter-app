import { call, put } from 'redux-saga/effects'
import RatesActions from '../Redux/RatesRedux'

export function * getRates (api) {
  const response = yield call(api.getRates)

  if (response.ok) {
    const date = response.data.time
    const rates = response.data.rates.reduce((rates, rate) => {
      rates[rate.currency] = Number(rate.rate)
      return rates
    }, {})
    rates[response.data.base] = 1

    yield put(RatesActions.ratesSuccess({ date, rates }))
  } else {
    yield put(RatesActions.ratesFailure())
  }
}
