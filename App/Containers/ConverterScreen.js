import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Text, View } from 'react-native'
import { RatesSelectors } from '../Redux/RatesRedux'
import Converter from '../Components/Converter'

// Styles
import styles from './Styles/ConverterScreenStyles'

class ConverterScreen extends Component {
  render () {
    return (
      <View style={styles.mainContainer}>
        <Text>Exchange Rates Updated on {this.props.date}</Text>
        <Converter rates={this.props.rates} />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  date: RatesSelectors.selectDate(state),
  rates: RatesSelectors.selectRates(state)
})

const mapDispatchToProps = dispatch => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(ConverterScreen)
