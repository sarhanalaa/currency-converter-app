import Dinero from 'dinero.js'

const create = (rates) => {
  const convert = (from, to, amount) => {
    if (!rates[from]) {
      throw `${from} was not found in given rates table`
    }

    if (!rates[to]) {
      throw `${from} was not found in given rates table`
    }

    return new Dinero({
      amount: Math.round(amount * 100)
    }).divide(rates[from])
      .multiply(rates[to])
      .toUnit()
  }

  return {
    convert
  }
}

export default {
  create
}
