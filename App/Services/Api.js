// a library to wrap and simplify api calls
import apisauce from 'apisauce'
import Config from 'react-native-config'

// our "constructor"
const create = (baseURL = Config.API_DOMAIN) => {
  const api = apisauce.create({
    baseURL,
    headers: {},
    timeout: 30000
  })

  const getRates = () => api.get(Config.API_ENDPOINT_RATES)

  return {
    getRates
  }
}

export default {
  create
}
