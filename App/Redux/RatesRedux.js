import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  ratesRequest: null,
  ratesSuccess: ['data'],
  ratesFailure: null
})

export const RatesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  error: null,
  date: null,
  rates: {}
})

/* ------------- Selectors ------------- */

export const RatesSelectors = {
  selectDate: state => state.rates.date,
  selectRates: state => state.rates.rates
}

/* ------------- Reducers ------------- */

// request the avatar for a user
export const request = (state) =>
  state.merge({ fetching: true })

// successful avatar lookup
export const success = (state, action) => {
  const { date, rates } = action.data
  return state.merge({ fetching: false, error: null, date, rates })
}

// failed to get the avatar
export const failure = (state) =>
  state.merge({ fetching: false, error: true })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RATES_REQUEST]: request,
  [Types.RATES_SUCCESS]: success,
  [Types.RATES_FAILURE]: failure
})
