import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, TextInput, View, Picker } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import numeral from 'numeral'
import styles from './Styles/ConverterStyles.js'
import CurrencyConverter from '../Services/CurrencyConverter'

export default class Converter extends Component {
  static propTypes = {
    rates: PropTypes.object.isRequired,
    onConvert: PropTypes.func
  }

  state = {
    sourceCurrency: 'EUR',
    targetCurrency: 'EUR',
    amount: 0,
    convertedAmount: 0
  }

  componentDidUpdate (prevProps) {
    if (this.props.rates) {
      this.currencyConverter = CurrencyConverter.create(this.props.rates)
    }
  }

  convertAmount () {
    const {
      sourceCurrency,
      targetCurrency,
      amount
    } = this.state

    const amountNumeral = numeral(amount)

    let convertedAmount = null
    if (sourceCurrency && targetCurrency && amountNumeral) {
      convertedAmount = numeral(
        this.currencyConverter.convert(
          sourceCurrency,
          targetCurrency,
          amountNumeral.value()
        )
      ).format('0,0.00')
    }

    this.setState({convertedAmount}, () => {
      if (this.props.onConvert) {
        this.props.onConvert(
          sourceCurrency,
          targetCurrency,
          amountNumeral.value(),
          convertedAmount.value()
        )
      }
    })
  }

  flipSelectedCurrencies () {
    const {
      sourceCurrency: targetCurrency,
      targetCurrency: sourceCurrency
    } = this.state

    this.setState({ sourceCurrency, targetCurrency }, this.convertAmount)
  }

  render () {
    const {
      sourceCurrency,
      targetCurrency,
      amount,
      convertedAmount
    } = this.state

    const currencies = Object.keys(this.props.rates).sort()

    return (
      <View style={styles.container}>
        <Text>Source</Text>
        <View style={styles.amountContainer}>
          <Picker
            style={styles.currencyPicker}
            selectedValue={sourceCurrency}
            onValueChange={sourceCurrency => this.setState({sourceCurrency}, this.convertAmount)}
          >
            {
              currencies.map(
                (currency, i) => <Picker.Item label={currency} value={currency} key={i} />
              )
            }
          </Picker>
          <TextInput
            keyboardType='numeric'
            value={String(amount)}
            onChangeText={amount => this.setState({amount}, this.convertAmount)}
            style={styles.amountText}
          />
        </View>

        <View style={styles.swapCurrenciesButton}>
          <Icon.Button
            name='swap-vertical'
            backgroundColor='#CCC'
            color='grey'
            onPress={() => this.flipSelectedCurrencies()}
          >
            Swap
          </Icon.Button>
        </View>

        <Text>Target</Text>
        <View style={styles.amountContainer}>
          <Picker
            style={styles.currencyPicker}
            selectedValue={targetCurrency}
            onValueChange={targetCurrency => this.setState({targetCurrency}, this.convertAmount)}
          >
            {
              this.props.rates &&
              currencies.map(
                (currency, i) => <Picker.Item label={currency} value={currency} key={i} />
              )
            }
          </Picker>

          <Text selectable style={styles.amountText}>{convertedAmount}</Text>
        </View>
      </View>
    )
  }
}
