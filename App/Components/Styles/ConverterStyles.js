import { StyleSheet } from 'react-native'
import { Metrics } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: Metrics.baseMargin
  },
  amountText: {
    fontSize: 32,
    textAlign: 'center',
    flex: 1
  },
  currencyPicker: {
    marginRight: Metrics.doubleBaseMargin,
    width: 120
  },
  amountContainer: {
    alignItems: 'center',
    flexDirection: 'row'
  },
  swapCurrenciesButton: {
    alignSelf: 'flex-start',
    marginVertical: Metrics.doubleBaseMargin,
    width: 100
  }
})
