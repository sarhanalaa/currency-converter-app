#  CurrencyConverterApp
Currency Converter App

ignited using [ignite-ir-boilerplate-andross](https://github.com/infinitered/ignite-ir-boilerplate-andross/tree/master/boilerplate)

## Get Started
1. Install dependencies `npm install` or `yarn`
2. Copy .env.example to .env
3. Run the app `react-native run-android` or `react-native run-ios`
