import CurrencyConverter from '../../App/Services/CurrencyConverter'

const rates = {
  EUR: 1,
  USD: 1.2,
  GBP: 0.8
}

const converter = CurrencyConverter.create(rates)

test('Converts From EUR', () => {
  expect(converter.convert('EUR', 'USD', 20)).toBe(24)
  expect(converter.convert('EUR', 'GBP', 20)).toBe(16)
})

test('Converts to EUR', () => {
  expect(converter.convert('USD', 'EUR', 40.80)).toBe(34)
  expect(converter.convert('GBP', 'EUR', 35.25)).toBe(44.06)
})

test('Converts two currencies', () => {
  expect(converter.convert('USD', 'GBP', 40)).toBe(26.66)
  expect(converter.convert('USD', 'GBP', 100.13)).toBe(66.75)

  expect(converter.convert('GBP', 'USD', 15)).toBe(22.50)
  expect(converter.convert('GBP', 'USD', 37.43)).toBe(56.15)
})
