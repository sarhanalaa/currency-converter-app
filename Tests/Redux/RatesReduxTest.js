import Actions, { reducer, INITIAL_STATE } from '../../App/Redux/RatesRedux'

test('request', () => {
  const state = reducer(INITIAL_STATE, Actions.ratesRequest())

  expect(state.fetching).toBe(true)
})

test('success', () => {
  const state = reducer(INITIAL_STATE, Actions.ratesSuccess({
    date: '2018-11-26',
    rates: {
      EUR: 1,
      GBP: 0.9,
      USD: 1.2
    }
  }))

  expect(state.fetching).toBe(false)
  expect(state.date).toBe('2018-11-26')
  expect(state.rates).toEqual({
    EUR: 1,
    GBP: 0.9,
    USD: 1.2
  })
  expect(state.error).toBeNull()
})

test('failure', () => {
  const state = reducer(INITIAL_STATE, Actions.ratesFailure())

  expect(state.fetching).toBe(false)
  expect(state.error).toBe(true)
})
