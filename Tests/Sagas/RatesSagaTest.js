import FixtureAPI from '../../App/Services/FixtureApi'
import { put, call } from 'redux-saga/effects'
import { getRates } from '../../App/Sagas/RatesSagas'
import RatesActions from '../../App/Redux/RatesRedux'

const stepper = (fn) => (mock) => fn.next(mock).value

test('first calls API', () => {
  const step = stepper(getRates(FixtureAPI))
  expect(step()).toEqual(call(FixtureAPI.getRates))
})

test('success path', () => {
  const step = stepper(getRates(FixtureAPI))
  step()
  const stepResponse = step(FixtureAPI.getRates())
  const expectedResponseData = {
    date: '2018-11-26',
    rates: {
      EUR: 1,
      GBP: 0.9,
      USD: 1.2
    }
  }
  expect(stepResponse).toEqual(put(RatesActions.ratesSuccess(expectedResponseData)))
})

test('failure path', () => {
  const step = stepper(getRates(FixtureAPI))
  step()
  const stepResponse = step({ok: false})
  expect(stepResponse).toEqual(put(RatesActions.ratesFailure()))
})
