import { put } from 'redux-saga/effects'
import { startup } from '../../App/Sagas/StartupSagas'
import RatesActions from '../../App/Redux/RatesRedux'

const stepper = (fn) => (mock) => fn.next(mock).value

test('watches for the right action', () => {
  const step = stepper(startup())
  RatesActions.ratesRequest()
  expect(step()).toEqual(put(RatesActions.ratesRequest()))
})
